import React, { useState } from "react";
import { useHistory } from "react-router-dom";


export default function Login() {
    const [username, setUserName] = useState();
    const [password, setPassword] = useState();
    let history = useHistory();



    const handleSubmit = () => {
        if (username === "test@test.com" && password === "test") {

            localStorage.setItem('token', '123456789')
            history.push("/")
        }
    }
    return (
        <div className="row">
            <div className="col-4 mx-auto my-2 text-center">

                <form onSubmit={handleSubmit} >
                    <h3>Sign In</h3>

                    <div className="form-group">
                        <input type="email" className="form-control" placeholder="Enter email" onChange={e => setUserName(e.target.value)} />
                    </div>

                    <div className="form-group">
                        <input type="password" className="form-control" placeholder="Enter password" onChange={e => setPassword(e.target.value)} />
                    </div>

                    <button className="btn btn-primary btn-block" >Login</button>

                </form>
            </div>
        </div>
    );
}
