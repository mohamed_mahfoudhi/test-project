import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import Login from "../login";
import './style.css'

export default function Home() {
    const [taskName, setTaskName] = useState('')
    const [description, setDescription] = useState('')
    const [taskList, setTaskList] = useState(JSON.parse(localStorage.getItem('task-list')) || [])
    const [status, setStatus] = useState("Non complétée")
    let history = useHistory();


    const addTask = async () => {
        await setTaskList(taskList => [...taskList, { taskName: taskName, description: description, status: status }]
        );
        let toStorage = JSON.stringify(taskList);
        localStorage.setItem('task-list', toStorage);
        setTaskName("");
        setDescription("");

    }
    const deleteTask = (index) => {
        const tasks = [...taskList];
        tasks.splice(index, 1);
        setTaskList(tasks);
        localStorage.setItem('task-list', JSON.stringify(tasks));

    };
    const logOut = () => {
        localStorage.removeItem("token")
        history.push("/login")
    }


    const token = localStorage.getItem('token');
    if (!token) {
        return <Login />
    }



    return (
        <div className="home container">
            <div className="top">
                <div>Créer une nouvelle tache</div>
                <button className="logout-btn" onClick={() => logOut()} >Logout</button>
            </div>
            <div className="home-createTache row">
                <div className="col-4 mx-auto my-2">
                    <div className="form-group">
                        <label>Nom de la tache</label>

                        <input type="email" className="form-control" placeholder="Nom de la tache" value={taskName} onChange={e => setTaskName(e.target.value)} />
                    </div>
                </div>
                <div className="col-4 mx-auto my-2">
                    <div className="form-group">
                        <label>Description de la tache en une ligne</label>

                        <input type="email" className="form-control" placeholder="Description de la tache en une ligne" value={description} onChange={e => setDescription(e.target.value)} />
                    </div>
                </div>
                <div className="col-4 mx-auto my-2">

                    <button className="btn btn-primary btn-block" onClick={addTask} >Ajouter la tache</button>
                </div>
            </div>
            <div className="home-listTaches">
                {taskList.map((item, index) => (
                    <div className="listTaches-item" key={index}
                    >
                        <div className="itemLeft">
                            <div className="listTaches-item-name">
                                {item.taskName}{" : "}
                            </div>
                            <div className="listTaches-item-description">
                                {item.description}
                            </div>
                            <button className="deleteBtn" onClick={() => deleteTask(index)} >Supprimer</button>
                        </div>
                        <div className="itemRight">{item.status}</div>
                    </div>
                ))}
            </div>
        </div>
    );
}
