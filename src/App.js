import React, { Component } from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Route, Switch } from "react-router-dom";
import Default from "./components/Default";
import Login from "./components/login";
import Home from "./components/home";
class App extends Component {

  render() {

    return (
      <React.Fragment>
        <Switch>
          <Route path="/login" component={Login} />
          <Route exact path="/" to="/home" component={Home} />
          <Route component={Default} />
        </Switch>
      </React.Fragment>
    );
  }
}

export default App;
